<?php

/**
 * @file
 * YouTube File Field support for use with the migrate module.
 */

/**
 * Implements hook_migrate_api().
 */
function youtube_file_field_migrate_api() {
  $api = array(
    'api' => 2,
    'field handlers' => array('MigrateYoutubeFileFieldHandler'),
  );
  return $api;
}

class MigrateYoutubeFileFieldHandler extends MigrateFieldHandler {

  /**
   * Declares the type(s) of fields used.
   */
  public function __construct() {
    $this->registerTypes(array('youtube_file_field'));
  }

  /**
   * Arguments for a youtube file field migration.
   *
   * @param string $url
   *   The URL of the youtube video.
   * @param integer $file
   *   The FID of the audio file, already migrated into Drupal.
   * @param boolean $upload
   *   Whether or not to upload files to youtube. Defaults to TRUE.
   *
   * @return array
   *   An array of the defined variables in this scope.
   */
  static function arguments($url = NULL, $file = NULL, $upload = TRUE) {
    return get_defined_vars();
  }

  /**
   * Implementation of MigrateFieldHandler::fields().
   *
   * @param $type
   *   The field type.
   * @param $instance
   *   Instance info for the field.
   * @param Migration $migration
   *   The migration context for the parent field. We can look at the mappings
   *   and determine which subfields are relevant.
   * @return array
   */
  public function fields($type, $instance, $migration = NULL) {
    return array(
      'url' => t('Subfield: The full youtube video URL'),
      'file' => t('Subfield: The FID of the video file.'),
      'upload' => t('Subfield: Whether or not to upload migrated files to youtube. Defaults to TRUE.')
    );
  }

  /**
   * Implements MigrateFieldHandler::prepare().
   *
   * @param $entity
   * @param array $field_info
   * @param array $instance
   * @param array $values
   *
   * @return null
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $arguments = array();
    if (isset($values['arguments'])) {
      $arguments = array_filter($values['arguments']);
      unset($values['arguments']);
      // If no value is set for "upload", assume TRUE.
      if (!isset($arguments['upload'])) {
        $arguments['upload'] = TRUE;
      }
    }
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    // If the migration doesn't put anything as the base field value, make an empty one.
    if (empty($values)) {
      $values[] = '';
    }

    // Setup the standard Field API array for saving. This is complicated because
    // subfield values are in $arguments with a different array structure.
    $delta = 0;
    foreach ($values as $value) {
      $return[$language][$delta] = $this->prepareArguments($arguments, $field_info, $delta)
        + array('url' => $value);

      // If a URL is specified, add the Video ID to the saved value.
      if (!empty($return[$language][$delta]['url'])) {
        $return[$language][$delta]['video_id'] = youtube_get_video_id($return[$language][$delta]['url']);
      }

      // Queue audio for upload if requested.
      if ($return[$language][$delta]['upload'] == TRUE && isset($return[$language][$delta]['fid'])) {
        // Get the queue.
        $queue = DrupalQueue::get('youtube_file_field_uploads');
        // Build the array to insert into the queue.
        $data = array(
          'type' => $instance['entity_type'],
          'bundle' => $instance['bundle'],
          'field' => $instance['field_name'],
          'fid' => $return[$language][$delta]['fid'],
        );
        // Insert it into the queue and save a queued timestamp.
        if ($queue->createItem($data)) {
          $return[$language][$delta]['queued'] = REQUEST_TIME;
        }
        // Remove the upload column.
        unset($return[$language][$delta]['upload']);
      }
      $delta++;
    }

    return isset($return) ? $return : NULL;
  }

  /**
   * Builds an array with additional data for the current $delta.
   *
   * @param  array $arguments
   * @param  array $field_info
   * @param  $delta
   *
   * @return array
   */
  protected function prepareArguments(array $arguments, array $field_info, $delta) {
    $result = array();
    $data = array();

    foreach ($arguments as $column_key => $column_value) {
      $value = NULL;

      if (is_array($arguments[$column_key])) {
        if (!empty($arguments[$column_key][$delta])) {
          $value = $arguments[$column_key][$delta];
        }
      }
      else {
        $value = $arguments[$column_key];
      }

      if ($value) {
        if (isset($field_info['columns'][$column_key]) || $column_key == 'upload') {
          // Store the data in a seperate column.
          $result[$column_key] = $value;
        }
        else {
          // Add the data to the 'data' column.
          $data[$column_key] = $value;
        }
      }
    }

    // Store all the other data as a serialized array in the data field.
    if (!empty($data)) {
      $result['data'] = serialize($data);
    }

    return $result;
  }
}
