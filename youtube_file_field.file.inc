<?php
/**
 * @file
 * Functions dealing with file module, setting permanent status, cleaning up, etc.
 */

/**
 * Implements hook_field_presave().
 * Set FILE_STATUS_PERMANENT on all files added through our widget.
 * @see file_field_presave();
 */
function youtube_file_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if (empty($item['file'])) {
      continue;
    }
    $file = file_load($item['file']);
    if (empty($file)) {
      continue;
    }
    if (!$file->status) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
    }
  }
}

/**
 * Implements hook_field_insert().
 * Add usage of uploaded files.
 * @see file_field_insert();
 */
function youtube_file_field_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Add a new usage of each uploaded file.
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  foreach ($items as $item) {
    if (!empty($item['file'])) {
      $file = (object) array('fid' => $item['file']);
      file_usage_add($file, 'file', $entity_type, $id);
    }
  }
}

/**
 * Implements hook_field_update().
 * When entities are updated, file_usage_add and file_usage_delete as appropriate.
 * @see file_field_update();
 */
function youtube_file_field_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Check whether the field is defined on the object.
  if (!isset($entity->{$field['field_name']})) {
    // We cannot check for removed files if the field is not defined.
    return;
  }

  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  // On new revisions, all files are considered to be a new usage and no
  // deletion of previous file usages are necessary.
  if (!empty($entity->revision)) {
    foreach ($items as $item) {
      if (isset($item['file'])) {
        $file = (object) array('fid' => $item['file']);
        file_usage_add($file, 'youtube_file_field', $entity_type, $id);
      }
    }
    return;
  }

  // Build a display of the current FIDs.
  $current_fids = array();
  foreach ($items as $item) {
    if (isset($item['file'])) {
      $current_fids[] = $item['file'];
    }
  }

  // Compare the original field values with the ones that are being saved. Use
  // $entity->original to check this when possible, but if it isn't available,
  // create a bare-bones entity and load its previous values instead.
  if (isset($entity->original)) {
    $original = $entity->original;
  }
  else {
    $original = entity_create_stub_entity($entity_type, array($id, $vid, $bundle));
    field_attach_load($entity_type, array($id => $original), FIELD_LOAD_CURRENT, array('field_id' => $field['id']));
  }

  // Build an array of original FIDs
  $original_fids = array();
  if (!empty($original->{$field['field_name']}[$langcode])) {
    foreach ($original->{$field['field_name']}[$langcode] as $original_item) {
      $original_fids[] = $original_item['file'];
      // If a given file from the original doesn't exist in the current FIDs array, decrement/delete its usage.
      if (isset($original_item['file']) && !in_array($original_item['file'], $current_fids)) {
        // Decrement the file usage count by 1 and delete the file if possible.
        file_field_delete_file($original_item['file'], $field, $entity_type, $id);
      }
    }
  }

  // If there were no files before, and no files after, return now.
  if (empty($current_fids) || !isset($item['file'])) {
    return;
  }

  // Add new usage entries for newly added files.
  foreach ($items as $item) {
    if (!in_array($item['file'], $original_fids)) {
      $file = (object) array('fid' => $item['file']);
      file_usage_add($file, 'youtube_file_field', $entity_type, $id);
    }
  }
}

/**
 * Implements hook_field_delete().
 * Delete files when an entity is deleted.
 * @see file_field_delete().
 */
function youtube_file_field_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  // Delete all file usages within this entity.
  foreach ($items as $delta => $item) {
    youtube_file_field_field_delete_file($item, $field, $entity_type, $id, 0);
  }
}

/**
 * Implements hook_field_delete_revision().
 * Delete files when an entity revision is deleted.
 * @see file_field_delete_revision().
 */
function youtube_file_field_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  foreach ($items as $delta => $item) {
    // Decrement the file usage count by 1 and delete the file if possible.
    if (youtube_file_field_field_delete_file($item, $field, $entity_type, $id)) {
      $items[$delta] = NULL;
    }
  }
}

/**
 * Decrements the usage count for a file and attempts to delete it.
 * @see file_field_delete_file()
 */
function youtube_file_field_field_delete_file($item, $field, $entity_type, $id, $count = 1) {
  // To prevent the field from deleting files it doesn't know about, check
  // the file reference count. Temporary files can be deleted because they
  // are not yet associated with any content at all.
  $file = file_load($item['file']);
  if ($file) {
    $file_usage = file_usage_list($file);
    if ($file->status == 0 || !empty($file_usage['youtube_file_field'])) {
      file_usage_delete($file, 'youtube_file_field', $entity_type, $id, $count);
      // Will only delete the file if the usage count is now 0.
      return file_delete($file);
    }
  }

  // Even if the file is not deleted, return TRUE to indicate the file field
  // record can be removed from the field database tables.
  return TRUE;
}