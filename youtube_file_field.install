<?php
/**
 * @file
 * Install/update hooks for youtube_file_field.
 */

/**
 * Implements hook_field_schema().
 */
function youtube_file_field_field_schema($field) {
  return array(
    'columns' => array(
      'url' => array(
        'type' => 'varchar',
        'length' => 1024,
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'video_id' => array(
        'type' => 'varchar',
        'length' => 15,
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 1024,
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'file' => array(
        'type' => 'int',
        'description' => 'File ID associated with the youtube video, if any',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
      'queued' => array(
        'type' => 'int',
        'description' => 'Timestamp from when the file was added to DrupalQueue, if any',
        'not null' => FALSE,
        'sortable' => TRUE,
      )
    ),
    'foreign_keys' => array(
      'file_usage' => array(
        'table' => 'file_usage',
        'columns' => array('file' => 'fid'),
      ),
    ),
  );
}

/**
 * [#46019] Update field schema to include the queued field.
 */
function youtube_file_field_update_7001() {
  // The schema of the field to be added.
  $field_schema = array(
    'type' => 'int',
    'description' => 'Timestamp from when the file was added to DrupalQueue, if any',
    'not null' => FALSE,
    'sortable' => TRUE,
  );

  // Get all field instances of type youtube_file_field.
  $fields = field_info_fields();
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'youtube_file_field' && $field['storage']['type'] == 'field_sql_storage') {
      // For each field, get the name of the db tables for storage and revisions.
      $tables = array(
        _field_sql_storage_tablename($field),
        _field_sql_storage_revision_tablename($field),
      );

      // One pass for storage, one for revision table.
      foreach ($tables as $table) {
        // Get the appropriate name in SQL for the column.
        $column_name = _field_sql_storage_columnname($field_name, 'queued');

        // Add the column.
        db_add_field($table, $column_name, $field_schema);
      }

      return t('Added %column column to %field.', array('%column' => 'queued', '%field' => $field_name));
    }
  }
  field_cache_clear();
}

/**
 * [#47252] Update field schema to include the title field.
 */
function youtube_file_field_update_7002() {
  // The schema of the field to be added.
  $field_schema = array(
    'type' => 'varchar',
    'length' => 1024,
    'description' => 'Video Title',
    'not null' => FALSE,
    'sortable' => TRUE,
  );

  // Get all field instances of type youtube_file_field.
  $fields = field_info_fields();
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'youtube_file_field' && $field['storage']['type'] == 'field_sql_storage') {
      // For each field, get the name of the db tables for storage and revisions.
      $tables = array(
        _field_sql_storage_tablename($field),
        _field_sql_storage_revision_tablename($field),
      );

      // One pass for storage, one for revision table.
      foreach ($tables as $table) {
        // Get the appropriate name in SQL for the column.
        $column_name = _field_sql_storage_columnname($field_name, 'title');

        // Add the column.
        db_add_field($table, $column_name, $field_schema);
      }

      return t('Added %column column to %field.', array('%column' => 'title', '%field' => $field_name));
    }
  }
  field_cache_clear();
}