<?php

/**
 * @file
 * Add a new field for uploading video files to Drupal, which will use Youtube for display.
 */

// Field module hooks are kept separate. Housekeeping FTW.
module_load_include('field.inc', 'youtube_file_field');
// Anything to do with handling files.
module_load_include('file.inc', 'youtube_file_field');

/**
 * Implements hook_cron_queue_info().
 * Declare our queue to be run asynchronously on cron.
 */
function youtube_file_field_cron_queue_info() {
  // Time limit is only checked between queue items, so if one video takes
  // longer than 600 seconds to upload, that's OK.
  $queues['youtube_file_field_uploads'] = array(
    'worker callback' => '_youtube_file_field_cron_process_file',
    'time' => 600,
  );
  return $queues;
}

/**
 * The actual cron process.
 * Check if the file still exists on the target entity, and generate a youtube URL.
 */
function _youtube_file_field_cron_process_file($data) {
  if ($data) {
    // Sanity check: does this field still exist on this bundle and type? Does the file exist?
    $file = file_load($data['fid']);
    if (!$file) {
      watchdog('youtube_file_field', 'Skipped processing file with FID %fid from field %field on %type bundle %bundle, because the file does not exist! It has been removed from the queue.', array(
        '%fid' => $data['fid'],
        '%field' => $data['field'],
        '%type' => $data['type'],
        '%bundle' => $data['bundle'],
      ), WATCHDOG_ERROR);
      return;
    }
    if (!field_info_instance($data['type'], $data['field'], $data['bundle'])) {
      $file_name = basename(drupal_realpath($file->uri));
      watchdog('youtube_file_field', 'Skipped processing file %name from field %field on %type bundle %bundle, because the field does not exist! It has been removed from the queue.', array(
        '%name' => $file_name,
        '%field' => $data['field'],
        '%type' => $data['type'],
        '%bundle' => $data['bundle'],
      ), WATCHDOG_ERROR);
      return;
    }

    // First check to see if the FID is still in use on any entity matching the description.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $data['type'])
      ->entityCondition('bundle', $data['bundle'])
      ->fieldCondition($data['field'], 'file', $data['fid'])
      ->age(FIELD_LOAD_CURRENT);
    $results = $query->execute();
    // Load up all the entities that match.
    if (isset($results[$data['type']])) {

      // Initialize the array of values to save.
      $value_to_save = array();

      foreach ($results[$data['type']] as $id => $result) {
        $entity = entity_load($data['type'], array($id));
        $wrapper = entity_metadata_wrapper($data['type'], $entity[$id]);
        // Is it a multi-value field?
        $field_info = field_info_field($data['field']);
        $is_multi_value = ($field_info['cardinality'] != 1);

        // It's the same logic whether it's multi-value or not, just insert the delta. (That's what she said)
        if ($is_multi_value) {
          foreach ($wrapper->{$data['field']} as $delta => $value) {
            // Make sure there isn't a URL already.
            if (empty($wrapper->{$data['field']}[$delta]->value()['url'])) {
              // Load the file.
              $file = file_load($data['fid']);
              if ($file) {
                // Build the title, description, etc from the field definition.
                $instance = field_info_instance($data['type'], $data['field'], $data['bundle']);
                $options = array(
                  'title' => token_replace($instance['settings']['title'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags')),
                  'description' => token_replace($instance['settings']['description'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags')),
                  'category' => $instance['settings']['category'],
                  'privacy_status' => $instance['settings']['privacy'],
                );
                // Titles are htmlencoded by default, which can mess up quotes.
                $options['title'] = html_entity_decode($options['title'], ENT_QUOTES);
                // But Youtube requires UTF8 valid titles
                $options['title'] = utf8_encode($options['title']);
                // Titles must be less than 100 characters.
                if (strlen($options['title']) > 99) {
                  $options['title'] = substr($options['title'], 0, 95) . '...';
                }
                // Tags take a little more work.
                $tags_with_commas = token_replace($instance['settings']['tags'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags'));
                // Explode into an array and trim.
                $options['tags'] = array_map('trim',explode('\n', $tags_with_commas));

                // Send it to Youtube.
                $video_id = youtube_file_field_upload_file($file, $options);
                if ($video_id) {
                  // Build the array value to save.
                  $value = $wrapper->{$data['field']}[$delta]->value();
                  $value['queued'] = NULL;
                  $value['video_id'] = $video_id;
                  $value['url'] = 'https://youtube.com/v/' . $video_id;

                  // Log it.
                  watchdog('youtube_file_field', 'Uploaded video file %uri from %type %id to %youtube', array(
                    '%uri' => $file->uri,
                    '%type' => $data['type'],
                    '%id' => $id,
                    '%youtube' => $value_to_save['url'],
                  ));

                  // Add the value to the array of values to save.
                  $value_to_save[$delta] = $value;
                }
                else {
                  $file_name = basename(drupal_realpath($file->uri));
                  $nid = $wrapper->getIdentifier();
                  watchdog('youtube_file_field', 'Failed to upload file %name from entity %id to Youtube.', array(
                    '%name' => $file_name,
                    '%id' => $nid
                  ), WATCHDOG_ERROR);
                }
              }
            }
          }
        }
        else {
          // Make sure there isn't a URL already.
          if (empty($wrapper->{$data['field']}->value()['url'])) {
            // Load the file.
            $file = file_load($data['fid']);
            if ($file) {
              // Build the title, description, etc from the field definition.
              $instance = field_info_instance($data['type'], $data['field'], $data['bundle']);
              $options = array(
                'title' => token_replace($instance['settings']['title'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags')),
                'description' => token_replace($instance['settings']['description'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags')),
                'category' => $instance['settings']['category'],
                'privacy_status' => $instance['settings']['privacy'],
              );
              // Titles are htmlencoded by default, which can mess up quotes.
              $options['title'] = html_entity_decode($options['title'], ENT_QUOTES);
              // But Youtube requires UTF8 valid titles
              $options['title'] = utf8_encode($options['title']);
              // Titles must be less than 100 characters.
              if (strlen($options['title']) > 99) {
                $options['title'] = substr($options['title'], 0, 95) . '...';
              }
              // Tags take a little more work.
              $tags_with_commas = token_replace($instance['settings']['tags'], array($data['type'] => reset($entity)), array('callback' => '_youtube_file_field_strip_tags'));
              // Explode into an array and trim.
              $options['tags'] = array_map('trim',explode(",", $tags_with_commas));

              // Send it to Youtube.
              try {
                  $video_id = youtube_file_field_upload_file($file, $options);
              }
              catch (Google_Exception $e) {
                throw new Exception('Google Exception encountered: ' . $e->getMessage() . '. Title: "' . $options['title'] . '"');
              }

              if ($video_id) {
                // Build the array value to save.
                $value_to_save = $wrapper->{$data['field']}->value();
                $value_to_save['queued'] = NULL;
                $value_to_save['video_id'] = $video_id;
                $value_to_save['url'] = 'https://youtube.com/v/' . $video_id;

                // Log it.
                watchdog('youtube_file_field', 'Uploaded video file %uri from %type %id to %youtube', array(
                  '%uri' => $file->uri,
                  '%type' => $data['type'],
                  '%id' => $id,
                  '%youtube' => $value_to_save['url'],
                ));
              }
              else {
                $file_name = basename(drupal_realpath($file->uri));
                $nid = $wrapper->getIdentifier();
                watchdog('youtube_file_field', 'Failed to upload file %name from node %id to Youtube.', array(
                  '%name' => $file_name,
                  '%id' => $nid
                ), WATCHDOG_ERROR);
              }
            }
          }
        }
        // If we have something to save, and it's different from the original.
        $original_value = $wrapper->{$data['field']}->value();
        if (!empty($value_to_save) && $value_to_save != $original_value) {
          $value_to_save += $original_value;
          // Actually set and save it.
          $wrapper->{$data['field']}->set($value_to_save);
          $wrapper->save();
        }
      }
    }
  }
}

/**
 * The magic happens here: upload a file to Youtube. Returns the Youtube video ID.
 * Much of this is cribbed from Google's doco:
 * https://developers.google.com/youtube/v3/code_samples/php#resumable_uploads
 *
 * @param stdClass $file
 * @param array $values
 *  An associative array of values to set on the uploaded youtube video. Allowed
 *  keys are:
 *   - title
 *   - description
 *   - tags (an array of tags)
 *   - category (Youtube Category ID, see https://developers.google.com/youtube/v3/docs/videoCategories/list .
 *   - privacy_status (acceptable values are "public", "private", and "unlisted")
 * @return string The youtube video ID.
 */
function youtube_file_field_upload_file($file, $values = array()) {
  $spoof_id = youtube_get_video_id(variable_get('youtube_file_field_spoof_video', ''));
  if (!empty($spoof_id)) {
    return $spoof_id;
  }
  // Get the Youtube connection object and client. Do not redirect.
  $youtube = youtube_api_get_loaded_youtube(FALSE);
  if ($youtube == NULL) {
    $file_name = basename(drupal_realpath($file->uri));
    watchdog('youtube_file_field', t('Could not upload the file %name to Youtube, because this site is not authorized yet.'), array('%name' => $file_name));
    return FALSE;
  }

  // Figure out our title/description/category. Sanitize them all just in case.
  global $base_url;
  $title = isset($values['title']) ? $values['title'] : variable_get('site_name', 'Video from ' . $base_url);
  $description = isset($values['description']) ? $values['description'] : variable_get('site_name', 'Video from ' . $base_url);
  $category = isset($values['category']) ? $values['category'] : 21;

  // Load the google autoloader by hand, to avoid class not found errors.
  $google_library = libraries_load('google-api-php-client');

  // Create a snippet with title, description, tags and category ID
  // Create an asset resource and set its snippet metadata and type.
  // This example sets the video's title, description, keyword tags, and
  // video category.
  $snippet = new Google_Service_YouTube_VideoSnippet();
  $snippet->setTitle($title);
  $snippet->setDescription($description);
  if (isset($values['tags'])) {
    $snippet->setTags($values['tags']);
  }

  // Numeric video category. See
  // https://developers.google.com/youtube/v3/docs/videoCategories/list
  // @TODO Get the list of these during authorization and offer the choice in field settings
  if (isset($values['category']) && is_int($values['category'])) {
    $snippet->setCategoryId($values['category']);
  }

  // Set the video's status to "public". Valid statuses are "public",
  // "private" and "unlisted".
  $status = new Google_Service_YouTube_VideoStatus();
  $status->privacyStatus = isset($values['privacy_status']) ? $values['privacy_status'] : "public";

  // Associate the snippet and status objects with a new video resource.
  $video = new Google_Service_YouTube_Video();
  $video->setSnippet($snippet);
  $video->setStatus($status);

  // Specify the size of each chunk of data, in bytes. Set a higher value for
  // reliable connection as fewer chunks lead to faster uploads. Set a lower
  // value for better recovery on less reliable connections.
  $chunkSizeM = variable_get('youtube_file_field_chunk_size', 1);
  // Double check that we have a valid integer in that variable.
  $chunkSizeM = is_integer($chunkSizeM) ? $chunkSizeM : 1;
  $chunkSizeBytes = $chunkSizeM * 1024 * 1024;

  // Setting the defer flag to true tells the client to return a request which can be called
  // with ->execute(); instead of making the API call immediately.
  $youtube->getClient()->setDefer(TRUE);

  // Set a lower curl timeout per https://stackoverflow.com/questions/31660589/how-to-set-timeout-for-google-api-php-client-library
  /*$youtube->getClient()->setClassConfig('Google_IO_Curl', 'options',
    array(
      CURLOPT_CONNECTTIMEOUT => 10,
      CURLOPT_TIMEOUT => 10
    )
  );*/

  // Create a request for the API's videos.insert method to create and upload the video.
  $insertRequest = $youtube->videos->insert("status,snippet", $video);

  // Create a MediaFileUpload object for resumable uploads.
  $media = new Google_Http_MediaFileUpload(
    $youtube->getClient(),
    $insertRequest,
    'video/*',
    NULL,
    TRUE,
    $chunkSizeBytes
  );
  $media->setFileSize($file->filesize);


  // Read the media file and upload it chunk by chunk.
  $status = FALSE;
  $handle = fopen($file->uri, "rb");
  while (!$status && !feof($handle)) {
    // Buffer reads, in case you're using a remote stream resource (ahem ahem - my use case)
    $buffer = '';
    while (strlen($buffer) < $chunkSizeBytes && !feof($handle)) { $buffer .= fread($handle, 8192); }

    // Apparently google's API service sometimes just times out?
    // @see https://code.google.com/p/gdata-issues/issues/detail?id=6581
    try {
      $status = $media->nextChunk($buffer);
    } catch (Exception $e) {
      if ($e->getCode() === 28) {
        continue;
      }
    }
  }

  fclose($handle);

  return isset($status->id) ? $status->id : FALSE;

}

/**
 * Implements hook_form_FORM_ID_alter().
 * Add a couple of fields to youtube_api's settings form
 * - Spoofed video ID
 * - Authorize now button
 */
function youtube_file_field_form_youtube_api_settings_form_alter(&$form, &$form_state, $form_id) {
  // Fieldset so it looks pretty.
  $form['youtube-file-field'] = array(
    '#type' => 'fieldset',
    '#title' => t('Youtube File Field settings'),
  );
  // Spoof video field. But you probably set this in settings.php, didn't you?
  $form['youtube-file-field']['youtube_file_field_spoof_video'] = array(
    '#title' => t('Fake video ID response from Youtube'),
    '#description' => t('Enter a Youtube video ID (not a URL). For use in development environments where you can\'t access the Youtube API. This will be returned for every file uploaded with youtube_file_field module.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('youtube_file_field_spoof_video', NULL),
  );

  // The submit handler that will do the authorization.
  $form['#submit'][] = 'youtube_file_field_authorize';

  // If we're not on HTTPS, don't let the user authorize... and make them feel bad about it.
  if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') {
    $form['authorize']['#disabled'] = TRUE;

    $form['authorize-message'] = array(
      '#markup' => t('You must access your site over HTTPS to authorize with Youtube!'),
      '#prefix' => '<div id="#authorize-message">',
      '#suffix' => '</div>',
    );
  }
  else {
    // Let the user know if the site is already authorized
    if (youtube_api_get_loaded_youtube(FALSE)) {
      $form['authorized-message'] = array(
        '#markup' => t('This site is already authorized with Youtube.'),
        '#prefix' => '<div id="#authorized-message">',
        '#suffix' => '</div>',
      );

      // Offer to refresh the Categories list, if appropriate.
      $form['refresh-categories'] = array(
        '#description' => t('Get an updated list of available categories from Youtube'),
        '#type' => 'submit',
        '#value' => t('Refresh Categories'),
        '#name' => 'refresh-categories',
      );
    }
  }

  // Authorize now, for the impatient.
  $form['authorize'] = array(
    '#description' => t('Save the form values and authorize with Youtube now.'),
    '#type' => 'submit',
    '#value' => t('Save and Authorize'),
    '#name' => 'authorize-now',
  );
}

/**
 * Receives submissions from the modified youtube_api admin form. Authorizes the site against youtube.
 */
function youtube_file_field_authorize($form, &$form_state) {
  switch ($form_state['triggering_element']['#name']) {
    case 'authorize-now':
      // If there was already a session, destroy the token and reacquire.
      // This helps users switch accounts (in order to prevent developers from having to intervene manually).
      if (variable_get('youtube_api_access_token')) {
        variable_del('youtube_api_access_token');
        watchdog('youtube authorization', t('Terminated existing YouTube session.'));
      }

      // Authorize the site with Youtube, redirecting if necessary.
      youtube_api_get_loaded_youtube(TRUE);
      break;

    case 'refresh-categories':
      $categories = array();
      $youtube = youtube_api_get_loaded_youtube(FALSE);
      $categories_response = $youtube->videoCategories->listVideoCategories('snippet', array('regionCode' => 'US'))
        ->getItems();
      foreach ($categories_response as $category) {
        $categories[$category->id] = $category->getSnippet()->getTitle();
      }
      variable_set('youtube_file_field_categories', $categories);
      break;
  }

}

/**
 * Implements of hook_theme().
 * Cribbed from the youtube module, but with some additions.
 */
function youtube_file_field_theme($existing, $type, $theme, $path) {
  return array(
    'youtube_thumbnail_with_title' => array(
      'variables' => array(
        'video_id' => NULL,
        'entity_title' => NULL,
        'image_style' => NULL,
        'image_link' => NULL,
        'title' => NULL,
        'audio' => NULL,
        'image_path' => NULL,
        'alt' => NULL,
      ),
      'file' => 'youtube_file_field.theme.inc',
    ),
  );
}

/**
 * Retrieves the title of a given video from the YouTube API.
 *
 * @param string $video_id - The ID of the video
 * @return bool|string
 */
function youtube_file_field_get_video_title($video_id) {
  $cid = 'youtube_file_field-video-' . $video_id;

  if (($cached = cache_get($cid)) !== FALSE) {
    return $cached->data;
  }

  // pull title and cache if it's valid
  $title = _youtube_file_field_retrieve_video_title($video_id);
  if ($title !== FALSE) {
    cache_set($cid, $title);
  }

  return $title;
}

/**
 * Download the video title directly from YouTube.
 *
 * @param $video_id - The ID of the video
 * @return bool|string
 */
function _youtube_file_field_retrieve_video_title($video_id) {
  $api_key = variable_get('youtube_api_key', NULL);

  $query = array(
    'id' => $video_id,
    'part' => 'snippet',
    'fields' => 'items(id,snippet(title))',
  );

  if (!empty($api_key)) {
    $query['key'] = $api_key;
  }

  $endpoint = url('https://www.googleapis.com/youtube/v3/videos', array(
    'external' => TRUE,
    'query' => $query,
  ));

  $response = drupal_http_request($endpoint);
  if ($response->code !== '200') {
    watchdog(
      'youtube_file_field',
      'Unable to retrieve metadata for video ID @id. Response: @code @status',
      array(
        '@id' => $video_id,
        '@code' => $response->code,
        '@status' => $response->status_message,
      ),
      WATCHDOG_NOTICE
    );

    return FALSE;
  }

  $result = json_decode($response->data, TRUE);
  if (json_last_error() !== JSON_ERROR_NONE) {
    watchdog(
      'youtube_file_field',
      'Unable to parse API response from youtube: @message',
      array(
        '@message' => json_last_error_msg(),
      ),
      WATCHDOG_NOTICE
    );

    return FALSE;
  }

  if (!empty($result['error'])) {
    watchdog(
      'youtube_file_field',
      'YouTube error: <code>@error</code>',
      array(
        '@error' => print_r($result['error'], TRUE),
      ),
      WATCHDOG_NOTICE
    );

    return FALSE;
  }

  if (isset($result['items'][0]['snippet']['title'])) {
    return $result['items'][0]['snippet']['title'];
  }

  return FALSE;
}

/**
 * Implements hook_cron().
 * Connect to the Youtube OAuth service to keep our keys alive
 */
function youtube_file_field_cron() {
  if (!youtube_api_get_loaded_youtube(FALSE)) {
    $config = l('configuration', 'admin/config/services/youtube-api');
    watchdog('youtube_file_field', 'Could not connect to Youtube on Cron. The site should be manually re-authorized on the %config page.', array('%config' => $config), WATCHDOG_WARNING);
  }
}

/**
 * Utility function to strip tags from token_replace output.
 */
function _youtube_file_field_strip_tags(&$token_results){
  foreach($token_results as $token => &$string) {
    $string = strip_tags($string);
  }
}