<?php

/**
 * @file
 * Field API stuff for youtube_file_field
 */

/**
 * Implements hook_field_info().
 */
function youtube_file_field_field_info() {
  return array(
    'youtube_file_field' => array(
      'label' => t('YouTube file field'),
      'description' => t('A video file saved in Drupal and hosted on YouTube.'),
      'default_widget' => 'youtube_file_field',
      'default_formatter' => 'youtube_file_field_video',
      'instance_settings' => array(
        'uri_scheme' => 'public',
        'file_directory' => '',
        'title' => '',
        'description' => '',
        'tags' => '',
        'privacy' => 'public',
        'category' => '',
        'file_resup' => FALSE,
      ),
      'property_type' => 'youtube_file_field',
      'property_callbacks' => array('youtube_file_field_property_info_callback'),
    ),
  );
}

/**
 * Property callback for this field type.
 * @see hook_field_info().
 */
function youtube_file_field_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $name = $field['field_name'];
  $property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$name];

  $property['type'] = ($field['cardinality'] != 1) ? 'list<youtube_file_field>' : 'youtube_file_field';
  $property['getter callback'] = 'entity_metadata_field_verbatim_get';
  $property['setter callback'] = 'entity_metadata_field_verbatim_set';
  $property['property info'] = youtube_file_field_field_data_property_info();

  unset($property['query callback']);
}


/**
 * Defines info for the properties of youtube_file_field data.
 */
function youtube_file_field_field_data_property_info($name = NULL) {
  return array(
    'input' => array(
      'label' => t('YouTube URL'),
      'description' => t('The absolute URL for the YouTube video.'),
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'video_id' => array(
      'label' => t('YouTube Video ID'),
      'description' => t('The ID assigned to the YouTube video'),
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'title' => array(
      'label' => t('Video Title'),
      'description' => t('The title of the YouTube video'),
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'file' => array(
      'label' => t('Video file'),
      'description' => t('The video file'),
      'type' => 'file',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'queued' => array(
      'label' => t('Queued timestamp'),
      'description' => t('The timestamp from when the video was queued to be uploaded'),
      'type' => 'integer',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 *
 * Pseudo-hook.
 */
function youtube_file_field_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = array();

  if ($field['type'] == 'youtube_file_field') {
    $scheme_options = array();
    foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
      $scheme_options[$scheme] = $stream_wrapper['name'];
    }
    $form['uri_scheme'] = array(
      '#type' => 'radios',
      '#title' => t('Upload destination'),
      '#options' => $scheme_options,
      '#default_value' => $settings['uri_scheme'],
      '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
    );

    $form['file_directory'] = array(
      '#type' => 'textfield',
      '#title' => t('File directory'),
      '#default_value' => $settings['file_directory'],
      '#description' => t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
      '#element_validate' => array('_file_generic_settings_file_directory_validate'),
    );

    $file_resup_link = l('File Resumable Upload', 'https://www.drupal.org/project/file_resup');
    $form['file_resup'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use File Resumable Uploads'),
      '#description' => t('Enable integration with !file_resup module.', array('!file_resup' => $file_resup_link)),
      '#default_value' => $settings['file_resup'],
      '#disabled' => !module_exists('file_resup'),
    );

    // If the site is not authorized...
    $youtube = youtube_api_get_loaded_youtube(FALSE);
    if (is_null($youtube)) {
      $guilt_trip = t('You must !authorize before you can configure this field!',
        array(
        '!authorize' => l('authorize the site with Youtube', 'admin/config/services/youtube-api'),
        )
      );

      $form['guilt'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('error')),
      );
        $form['guilt']['markup'] = array(
        '#markup' => $guilt_trip,
        '#value' => '',
        '#required' => TRUE,
        '#element_validate' => array('youtube_file_field_field_settings_validate'),
      );
      // Set an error to be sure they see it.
      drupal_set_message($guilt_trip, 'error');
    }
    else {

      // Build a list of token types that make sense for this field instance.
      $token_types = array('global');
      // Token type names != entity type names, so there's a mapping function.
      $token_type_for_this_entity_type = token_get_entity_mapping('entity', $instance['entity_type']);
      if ($token_type_for_this_entity_type) {
        $token_types[] = $token_type_for_this_entity_type;
      }

      $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Youtube title'),
        '#description' => t('The Title to be used on Youtube for uploaded videos. This field supports tokens; see the list of available tokens below.'),
        '#element_validate' => array('token_element_validate'),
        '#default_value' => $settings['title'],
        '#token_types' => $token_types,
        '#required' => TRUE,
      );

      $form['description'] = array(
        '#type' => 'textarea',
        '#title' => t('Youtube description'),
        '#description' => t('The Description to be used on Youtube for uploaded videos. This field supports tokens; see the list of available tokens below.'),
        '#default_value' => $settings['description'],
        '#token_types' => $token_types,
      );

      $form['tags'] = array(
        '#type' => 'textarea',
        '#title' => t('Youtube tags'),
        '#description' => t('List of Tags to be used on Youtube for uploaded videos. One tag per line. This field supports tokens; see the list of available tokens below.'),
        '#element_validate' => array('token_element_validate'),
        '#default_value' => $settings['tags'],
        '#token_types' => $token_types,
      );

      $form['tokens'] = array(
        '#theme' => 'token_tree_link',
        '#token_types' => $token_types,
      );

      $form['privacy'] = array(
        '#type' => 'select',
        '#title' => t('Youtube privacy setting'),
        '#description' => t('The youtube privacy mode for uploaded videos.'),
        '#options' => array(
          'public' => 'public',
          'private' => 'private',
          'unlisted' => 'unlisted'
        ),
        '#default_value' => $settings['privacy'],
      );

      // If we have a cached list of Youtube categories, use that
      $categories = variable_get('youtube_file_field_categories', array());

      // If we don't have any categories, pull them from Youtube.
      if (empty($categories)) {
        $categories_response = $youtube->videoCategories->listVideoCategories('snippet', array('regionCode' => 'US'))
          ->getItems();
        foreach ($categories_response as $category) {
          $categories[$category->id] = $category->getSnippet()->getTitle();
        }
        variable_set('youtube_file_field_categories', $categories);
      }

      $form['category'] = array(
        '#type' => 'select',
        '#title' => t('Youtube category'),
        '#description' => t('The youtube category for uploaded videos.'),
        '#options' => $categories,
        '#default_value' => $settings['category'],
      );
    }
  }
  return $form;
}


function youtube_file_field_field_settings_validate($element, &$form_state, $form) {
  $categories = variable_get('youtube_file_field_categories', FALSE);

  if ($categories === FALSE) {
    $guilt_trip = t('You must !authorize before you can configure this field!', array(
      '!authorize' => l('authorize the site with Youtube', 'admin/config/services/youtube-api'
      )));
    form_set_error('guilt', $guilt_trip);
  }
}

  /**
 * Implements hook_field_widget_info().
 * Add our Youtube Upload widget.
 */
function youtube_file_field_field_widget_info() {
  $info['youtube_file_field'] = array(
    'label' => t('Youtube Upload'),
    'field types' => array('youtube_file_field'),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );
  return $info;
}

/**
 * Implements hook_field_widget_form().
 * The actual field widget to be displayed in forms.
 */
function youtube_file_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'youtube_file_field') {

    // Load the items for form rebuilds from the field state as they might not be
    // in $form_state['values'] because of validation limitations. Also, they are
    // only passed in as $items when editing existing entities.
    $field_state = field_form_get_state($element['#field_parents'], $field['field_name'], $langcode, $form_state);
    if (isset($field_state['items'])) {
      $items = $field_state['items'];
    }

    // Put it all into a fieldset so it looks purdy.
    $base = $element;
    $element += array(
      '#type' => 'fieldset',
      'collapsible' => FALSE,
      // Allows us to submit an array as a value.
      '#extended' => TRUE,
      '#description' => t('If you provide a file and no URL, the file will be uploaded to Youtube automatically, and a URL will be generated for you.'),
      '#element_validate' => array('youtube_file_field_input_validate'),
    );
    // Essentially we use the managed_file type, extended with some enhancements.
    $element['file'] = array(
        '#title' => t('Video file'),
        '#description' => t('Youtube accepts mov, mpeg4, mp4, avi, wmv, mpegps, flv, 3gpp, and webm files, up to 128Gb.'),
        '#type' => 'managed_file',
        '#upload_location' => $instance['settings']['uri_scheme'] . '://' . $instance['settings']['file_directory'],
        '#upload_validators' => array(
          'file_validate_extensions' => array('mov mpeg4 mp4 avi wmv mpegps flv 3gpp webm'),
          'file_validate_size' => array(128*1024*1024*1024),
        ),
        '#process' => array('file_managed_file_process'),
        '#progress_indicator' => 'bar',
        '#default_value' => isset($items[$delta]['file']) ? $items[$delta]['file'] : NULL,
        '#weight' => -1,
      ) + $base;
    // file_resup settings, if enabled and available.
    if (module_exists('file_resup') && user_access('upload via file_resup') && file_upload_max_size() >= file_resup_chunksize()
      && !empty($instance['settings']['file_resup']) && $instance['settings']['file_resup']) {
      // Most file_resup settings belong on the parent element.
      $element['#file_resup_max_files'] = 1;
      $element['#field_name'] = $field['field_name'];

      // Validators are read from both the parent and the child, so set in both places.
      $element['file']['#file_resup_upload_validators'] = $element['file']['#upload_validators'];
      $element['#file_resup_upload_validators'] = $element['file']['#upload_validators'];

      // Set our version of the file_resup #process callback.
      $element['#process'] = array('youtube_file_field_resup_widget_process');
      // We can use file_resup's own field_value callback.
      $element['file']['#file_value_callbacks'][] = 'file_resup_field_widget_value';

      // This is called from both the parent and the child. It's already set in the child, add it to the parent.
      $element['#upload_location'] = $instance['settings']['uri_scheme'] . '://' . $instance['settings']['file_directory'];
    }

    // Youtube URL.

    $url_present = isset($items[$delta]['url']) && !empty($items[$delta]['url']);
    $queued_present = isset($items[$delta]['queued']) && !empty($items[$delta]['queued']);

    if ($url_present || !$queued_present) {
      $element['url'] = array(
        '#title' => t('Youtube URL'),
        '#type' => 'textfield',
        '#default_value' => $url_present ? $items[$delta]['url'] : NULL,
        '#description' => t('Leave this blank to have your file sent to Youtube, and the URL generated automatically for you.'),
        '#weight' => 0,
      );
    }
    else {

      // Queued timestamp
      if ($queued_present) {
        $queued_date = format_date($items[$delta]['queued'], 'short');
        $element['queued'] = array(
          '#prefix' => '<div class="youtube-file-field-queued">',
          '#markup' => t('This video was queued for upload at !queued', array('!queued' => $queued_date)),
          '#suffix' => '</div>',
          '#weight' => 1,
          '#value' => $items[$delta]['queued'],
        );
      }
    }

    // Youtube ID.
    if (isset($items[$delta]['video_id'])) {
      $element['video_id'] = array(
        '#prefix' => '<div class="youtube-video-id">',
        '#markup' => t('YouTube video ID: !video_id', array('!video_id' => $items[$delta]['video_id'])),
        '#suffix' => '</div>',
        '#weight' => 1,
        '#value' => $items[$delta]['video_id'],
      );
    }
  }

  return $element;
}

/**
 * Processor for the file resup widget.
 * @see file_resup_field_widget_process().
 */
function youtube_file_field_resup_widget_process($element, &$form_state, $form)  {
  $path = drupal_get_path('module', 'file_resup');
  $max_files = $element['#file_resup_max_files'];

  // Autostart is always false, because we don't have a setting for it.
  $autostart = FALSE;

  // Get the upload validators and build a new description.
  if (isset($element['#bundle'])) {
    $field = field_widget_field($element, $form_state);
    $instance = field_widget_instance($element, $form_state);
    $description = $field['cardinality'] == 1 ? field_filter_xss($instance['description']) : '';
  }
  else {
    $description = $element['#description'];
  }
  $upload_validators = $element['file']['#file_resup_upload_validators'];
  $description = theme('file_upload_help', array('description' => $description, 'upload_validators' => $upload_validators));
  // Add the resup element.
  // NB: some of these originally depended on file_managed_file_process running first... which won't happen with a child field.
  // In those cases I tried to derive the value the same way in here instead.
  $element['file']['resup'] = array(
    '#type' => 'hidden',
    '#value_callback' => 'file_resup_value',
    '#field_name' => $element['#field_name'],
    '#field_parents' => $element['#field_parents'],
    '#upload_location' => $element['file']['#upload_location'],
    '#file_resup_upload_validators' => $upload_validators,
    '#attributes' => array(
      'class' => array('file-resup'),
      'data-upload-name' => 'files[' . implode('_', $element['#parents']) . '_file]',
      'data-upload-button-name' => implode('_', $element['#parents']) . '_file_upload_button',
      'data-max-filesize' => $upload_validators['file_validate_size'][0],
      'data-description' => $description,
      'data-url' => url('file_resup/upload/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value']),
      'data-drop-message' => $max_files > -1 ? format_plural($max_files, 'Drop a file here or click <em>Browse</em> below.', 'Drop up to @count files here or click <em>Browse</em> below.') : t('Drop files here or click <em>Browse</em> below.'),
    ),
    '#prefix' => '<div class="file-resup-wrapper">',
    '#suffix' => '</div>',
    '#attached' => array(
      'css' => array($path . '/file_resup.css'),
      'js' => array(
        $path . '/js/resup.js',
        $path . '/file_resup.js',
        array(
          'type' => 'setting',
          'data' => array('file_resup' => array('chunk_size' => file_resup_chunksize())),
        ),
      ),
    ),
  );

  // Set the upload location on the parent element, because that's the only change needed to use file_resup's own uploading menu callback.
  $element['#upload_location'] = $element['file']['#upload_location'];

  // Add the extension list as a data attribute.
  if (isset($upload_validators['file_validate_extensions'][0])) {
    $extension_list = implode(',', array_filter(explode(' ', $upload_validators['file_validate_extensions'][0])));
    $element['file']['resup']['#attributes']['data-extensions'] = $extension_list;
  }

  // Add the maximum number of files as a data attribute.
  if ($max_files > -1) {
    $element['file']['resup']['#attributes']['data-max-files'] = $max_files;
  }

  // Add autostart as a data attribute.
  if ($autostart) {
    $element['file']['resup']['#attributes']['data-autostart'] = 'on';
  }

  $element['file']['upload_button']['#submit'][] = 'youtube_file_field_resup_widget_submit';
  $element['file']['#pre_render'][] = 'youtube_file_field_resup_widget_pre_render';

  return $element;
}

/**
 * #pre_render callback for the field widget element.
 */
function youtube_file_field_resup_widget_pre_render($element) {
  if (!empty($element['file']['#value']['fid'])) {
    $element['file']['resup']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Our version of file_resup_widget_submit.
 * Identical except that it uses another method to get the field status.
 * @see file_resup_widget_submit().
 */
function youtube_file_field_resup_widget_submit($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);

  if (!empty($field_state)) {
    $items = $field_state['items'];
  }
  elseif (!empty($form_state['values'][$field_name])) {
    $items[] = array('fid' => $form_state['values'][$field_name]);
    // If it's an extended field, look for subfield name variable and use that.
    if (isset($form[$field_name]['#extended']) && $form[$field_name]['#extended'] && !empty($element['#subfield_name'])) {
      $subfield_name = $element['#subfield_name'];
      if (!empty($form_state['values'][$field_name][$subfield_name])) {
        $items[] = array('fid' => $form_state['values'][$field_name][$subfield_name]);
      }

    }
  }

  // Remove possible duplicate items.
  $fids = array();
  foreach ($items as $delta => $item) {
    if (in_array($item['fid'], $fids)) {
      unset($items[$delta]);
    }
    else {
      $fids[] = $item['fid'];
    }
  }
  $items = array_values($items);

  // Append our items.
  if (!empty($element['resup']['#value'])) {
    $fids = array_diff(explode(',', $element['resup']['#value']), $fids);
    foreach ($fids as $fid) {
      $items[] = array('fid' => $fid);
    }
  }

  drupal_array_set_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2), $items);
  $field_state['items'] = $items;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
}


/**
 * Validation for the youtube field itself.
 */
function youtube_file_field_input_validate($element, &$form_state, $form) {
  // form_set_value expects to save an array as our field value. Get it started.
  $value_to_save = array(
    'file' => $element['file']['#value']['fid'],
  );

  // If there's no URL.
  if (!isset($element['url']) || empty($element['url']['#value'])) {
    // If it hasn't been queued yet, queue it up!
    if (!isset($element['queued'])) {
      // Load the actual file from FID.
      $file = file_load($element['file']['#value']['fid']);
      if ($file) {
        // Add the file to the queue, and put the timestamp value in.
        $queue = DrupalQueue::get('youtube_file_field_uploads');
        // Values to insert into the queue.
        $data = array(
          'type' => $element['#entity_type'],
          'bundle' => $element['#bundle'],
          'field' => $element['#field_name'],
          'fid' => $file->fid,
        );
        // Insert them into the queue.
        if ($queue->createItem($data)) {
          $value_to_save['queued'] = REQUEST_TIME;
        }
      }
    }
    // If it HAS been queued, preserve the queued timestamp.
    else {
      $value_to_save['queued'] = $element['queued']['#value'];
    }
  }
  // If there IS a URL.
  else {
    // Explicitly set a null queued timestamp.
    $value_to_save['queued'] = NULL;
    // Get the ID value from the URL.
    $url = $element['url']['#value'];
    $video_id = youtube_get_video_id($url);
    // Only save the URL and ID if it was a real youtube URL.
    if ($video_id) {
      $value_to_save['video_id'] = $video_id;
      $value_to_save['url'] = $url;
      // make call to add title
      $title = youtube_file_field_get_video_title($video_id);
      $value_to_save['title'] = $title;
    }
  }
  // Save whatever field value we ended up with.
  form_set_value($element, $value_to_save, $form_state);
}

/**
 * Implements hook_field_is_empty().
 */
function youtube_file_field_field_is_empty($item, $field) {
  return (empty($item['url']) && empty($item['file']));
}

/**
 * Implements hook_field_formatter_info().
 * Wraps around youtube module.
 */
function youtube_file_field_field_formatter_info() {
  // We just take the settings from Youtube module's formatter.
  $youtube_module_formatter_info = youtube_field_formatter_info();
  $formatters['youtube_file_field_video'] = $youtube_module_formatter_info['youtube_video'];
  $formatters['youtube_file_field_thumbnail'] = $youtube_module_formatter_info['youtube_thumbnail'];
  // Our formatter is only valid for our field.
  foreach ($formatters as $delta => $formatter) {
    $formatters[$delta]['field types'] = array('youtube_file_field');
  }

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 * @see youtube_field_formatter_settings_form()
 */
function youtube_file_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'youtube_file_field_video') {
    $element['youtube_size'] = array(
      '#type' => 'select',
      '#title' => t('YouTube video size'),
      '#options' => youtube_size_options(),
      '#default_value' => $settings['youtube_size'],
    );
    $element['youtube_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 10,
      '#default_value' => $settings['youtube_width'],
      '#states' => array(
        'visible' => array(
          ':input[name="fields[' . $field['field_name'] . '][settings_edit_form][settings][youtube_size]"]' => array('value' => 'custom'),
        ),
      ),
    );
    $element['youtube_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 10,
      '#default_value' => $settings['youtube_height'],
      '#states' => array(
        'visible' => array(
          ':input[name="fields[' . $field['field_name'] . '][settings_edit_form][settings][youtube_size]"]' => array('value' => 'custom'),
        ),
      ),
    );
    $element['youtube_autoplay'] = array(
      '#type' => 'checkbox',
      '#title' => t('Play video automatically when loaded (autoplay).'),
      '#default_value' => $settings['youtube_autoplay'],
    );
    $element['youtube_loop'] = array(
      '#type' => 'checkbox',
      '#title' => t('Loop the playback of the video (loop).'),
      '#default_value' => $settings['youtube_loop'],
    );
    $element['youtube_showinfo'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide video title and uploader info (showinfo).'),
      '#default_value' => $settings['youtube_showinfo'],
    );
    $element['youtube_controls'] = array(
      '#type' => 'checkbox',
      '#title' => t('Always hide video controls (controls).'),
      '#default_value' => $settings['youtube_controls'],
    );
    $element['youtube_autohide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide video controls after play begins (autohide).'),
      '#default_value' => $settings['youtube_autohide'],
    );
    $element['youtube_iv_load_policy'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide video annotations by default (iv_load_policy).'),
      '#default_value' => $settings['youtube_iv_load_policy'],
    );
  }

  if ($display['type'] == 'youtube_file_field_thumbnail') {
    $element['image_style'] = array(
      '#type' => 'select',
      '#title' => t('Image style'),
      '#options' => image_style_options(FALSE),
      '#default_value' => $settings['image_style'],
      '#empty_option' => t('None (original image)'),
    );

    // Option to link the thumbnail to its original node, the YouTube video, or
    // (if the youtube_colorbox is enabled) a Colorbox modal window.
    $element['image_link'] = array(
      '#title' => t('Link image to'),
      '#type' => 'select',
      '#default_value' => $settings['image_link'],
      '#empty_option' => t('Nothing'),
      '#options' => youtube_thumbnail_link_types(),
    );

    if (module_exists('youtube_colorbox')) {
      // Add Colorbox settings to this form.
      youtube_colorbox_thumbnail_field_formatter_settings($element, $instance, $settings, $field['field_name']);
    }
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 * @see youtube_field_formatter_settings_summary().
 */
function youtube_file_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  // Summary for the video style.
  if ($display['type'] == 'youtube_file_field_video') {
    $video_sizes = youtube_size_options();
    if (isset($video_sizes[$settings['youtube_size']])) {
      $summary = t('YouTube video: @size', array('@size' => $video_sizes[$settings['youtube_size']]));
    }
    else {
      $summary = t('YouTube video: 450px by 315px');
    }

    $parameters = array(
      $settings['youtube_autoplay'],
      $settings['youtube_loop'],
      $settings['youtube_showinfo'],
      $settings['youtube_controls'],
      $settings['youtube_autohide'],
      $settings['youtube_iv_load_policy'],
    );

    foreach ($parameters as $parameter) {
      if ($parameter) {
        $summary .= t(', custom parameters');
        break;
      }
    }
    return $summary;
  }

  // Summary for the thumbnail style.
  if ($display['type'] == 'youtube_file_field_thumbnail') {
    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    if (isset($image_styles[$settings['image_style']])) {
      $summary = t('Image style: @style.', array('@style' => $image_styles[$settings['image_style']]));
    }
    else {
      $summary = t('Original image.');
    }

    // Display this setting only if image is linked.
    $link_types = youtube_thumbnail_link_types();
    if (isset($settings['image_link']) && isset($link_types[$settings['image_link']])) {
      $summary .= '<br/>' . t('Linked to: ') . $link_types[$settings['image_link']] . '.';
    }

    return $summary;
  }
}

/**
 * Implements hook_field_formatter_view().
 * Copied from youtube module, but returns empty if the item has no video.
 * @see youtube_field_formatter_view()
 */
function youtube_file_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  if ($display['type'] == 'youtube_file_field_video' || $display['type'] == 'youtube_file_field_thumbnail') {
    drupal_add_js('https://www.youtube.com/iframe_api', 'external');
  }

  switch ($display['type']) {
    // This formatter outputs the youtube embed code.
    case 'youtube_file_field_video':
      foreach ($items as $delta => $item) {
        if (!empty($item['url'])) {
          $element[$delta] = array(
            '#theme' => 'youtube_video',
            '#video_id' => $item['video_id'],
            '#entity_title' => !empty($entity->title) ? check_plain($entity->title) : NULL,
            '#size' => array_key_exists('youtube_size', $display['settings']) ? $display['settings']['youtube_size'] : NULL,
            '#width' => array_key_exists('youtube_width', $display['settings']) ? $display['settings']['youtube_width'] : NULL,
            '#height' => array_key_exists('youtube_height', $display['settings']) ? $display['settings']['youtube_height'] : NULL,
            '#autoplay' => array_key_exists('youtube_autoplay', $display['settings']) ? $display['settings']['youtube_autoplay'] : FALSE,
            '#loop' => array_key_exists('youtube_loop', $display['settings']) ? $display['settings']['youtube_loop'] : FALSE,
            '#showinfo' => array_key_exists('youtube_showinfo', $display['settings']) ? $display['settings']['youtube_showinfo'] : FALSE,
            '#controls' => array_key_exists('youtube_controls', $display['settings']) ? $display['settings']['youtube_controls'] : FALSE,
            '#autohide' => array_key_exists('youtube_autohide', $display['settings']) ? $display['settings']['youtube_autohide'] : FALSE,
            '#iv_load_policy' => array_key_exists('youtube_iv_load_policy', $display['settings']) ? $display['settings']['youtube_iv_load_policy'] : FALSE,
          );
        }
      }
      break;

    // This formatter uses an imagecache preset to generate a thumbnail.
    case 'youtube_file_field_thumbnail':
      if (!empty($items[0]['url'])) {
        // Check if the formatter involves a link.
        if (isset($display['settings']['image_link'])) {
          switch ($display['settings']['image_link']) {
            case 'content':
              $uri = entity_uri($entity_type, $entity);
              $uri['options']['html'] = TRUE;
              break;
            case 'youtube':
              $link_youtube = TRUE;
              break;
            case 'colorbox':
              $link_colorbox = TRUE;
              break;
          }
        }

        foreach ($items as $delta => $item) {
          // If the thumbnail is linked to it's youtube page, take the original url.
          if (!empty($link_youtube)) {
            $uri = array(
              'path' => $item['input'],
              'options' => array('html' => TRUE),
            );
          }

          // Add support for the colorbox module.
          if (module_exists('youtube_colorbox') && !empty($link_colorbox)) {
            // Always open in an iframe for proper origin access.
            if (!empty($display['settings']['colorbox']['parameters'])) {
              $display['settings']['colorbox']['parameters']['iframe'] = TRUE;
            }

            $uri = youtube_colorbox_field_item_uri($item, $display['settings']);
          }

          $id = $item['video_id'];
          $image_style = $display['settings']['image_style'];

          // create the thumbnail
          $files = variable_get('file_public_path', conf_path() . '/files');
          $youtube_dir = variable_get('youtube_thumb_dir', 'youtube');
          $file_path = $files . '/' . $youtube_dir . '/' . $id . '.jpg';

          // Attempt to construct local file
          if (!file_exists($file_path)) {
            if (!youtube_get_remote_image($id)) {
              // use remote location if copy failed
              $file_path = youtube_build_remote_image_path($id);

              // we can't use image styles on remote elements
              $image_style = NULL;
            }
          }

          // image styles use a slightly different path scheme
          if ($image_style && !url_is_external($file_path)) {
            $file_path = 'public://' . $youtube_dir . '/' . $id . '.jpg';
          }

          if (empty($item['title'])) {
            $alt = t('Video thumbnail');
          } else {
            $alt = t('Video thumbnail for @title', array(
              '@title' => $item['title'],
            ));
          }

          $element[$delta] = array(
            '#theme' => 'youtube_thumbnail_with_title',
            '#video_id' => $id,
            '#entity_title' => !empty($entity->title) ? check_plain($entity->title) : NULL,
            '#image_style' => $image_style,
            '#image_link' => isset($uri) ? $uri : '',
            '#image_path' => $file_path,
            '#audio' => !empty($entity->field_soundcloud_audio),
            '#title' => $item['title'],
            '#alt' => $alt,
          );
        }
      }
      break;
  }

  return $element;
}