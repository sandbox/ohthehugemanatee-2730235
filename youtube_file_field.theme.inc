<?php

/**
* Theme function for thumbnails with title.
*/
function theme_youtube_thumbnail_with_title($variables) {
  $style = $variables['image_style'];
  $path = $variables['image_path'];
  $alt = $variables['alt'];

  if ($style) {
    $image = theme('image_style', array(
      'style_name' => $style,
      'path' => $path,
      'alt' => $alt,
    ));
  }
  else {
    $image = theme('image', array(
      'path' => $path,
      'alt' => $alt,
    ));
  }

  $classes = array(
    'teaser',
    'teaser--playlist',
    'has-video',
  );

  if ($variables['audio']) {
    $classes[] = 'has-audio';
  }

  $output = '';
  $output .= '<span class="' . join(' ', $classes) . '">';
  $output .= '<span class="teaser__media">' . $image . '</span>';
  $output .= '<span class="teaser__title">' . $variables['title'] . '</span>';
  $output .= '</span>';

  return $output;
}